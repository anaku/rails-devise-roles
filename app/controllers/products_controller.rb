class ProductsController < ApplicationController

  def index
    #raise current_user.inspect
    @product = Product.all
  end

  def new
  end

  def create
    @product = Product.new(product_params)

    @product.save
    redirect_to @product
  end

  def show
    @product = Product.find(params[:id])
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])

    if @product.update(product_params)
      redirect_to @product
    end
  end

  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    redirect_to products_path
  end

  private
  def product_params
    params.require(:product).permit(:name, :description, :user_id)
  end
end
